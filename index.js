// "document" refers to the whole webpage
// "querySelector" is used to select a speicif object(HTML ELEMENT) from our document (WEBPAGE)
const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name")
const txtLastName = document.querySelector("#txt-last-name");

//document.getElementByID
//document.getElementByClassName


//SIMPLE WAY

/*txtFirstName.addEventListener('keyup',(event) => {
	// "innerHTML" property sets or returns the HTML content 
	spanFullName.innerHTML = event.target.value;
	console.log(event.target);
	console.log(event.target.value)
})

txtLastName.addEventListener('keyup',(event) => {
	// "innerHTML" property sets or returns the HTML content 
	spanFullName.innerHTML = txtFirstName.value + " " +event.target.value;
	console.log(event.target);
	console.log(event.target.value)
})*/

txtFirstName.addEventListener('keyup', (event) => {
  const fullName = txtLastName.value ? `${event.target.value} ${txtLastName.value}` : event.target.value;
  spanFullName.innerHTML = fullName;
  console.log(event.target);
  console.log(event.target.value);
});

txtLastName.addEventListener('keyup', (event) => {
  const fullName = txtFirstName.value ? `${txtFirstName.value} ${event.target.value}` : event.target.value;
  spanFullName.innerHTML = fullName;
  console.log(event.target);
  console.log(event.target.value);
});